#!/bin/bash

BASEDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# BASH
ln -svf ${BASEDIR}/.bash_profile ~/.bash_profile

# GIT
ln -svf ${BASEDIR}/.gitconfig ~/.gitconfig
ln -svf ${BASEDIR}/.gitignore_global ~/.gitignore_global

# NPM
ln -svf ${BASEDIR}/.npmrc ~/.npmrc

source ${BASEDIR}/scripts/osx_defaults.sh
source ~/.bash_profile
